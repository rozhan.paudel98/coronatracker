import React from 'react';
import NavBar from "./components/NavBar/navbar"
import {BrowserRouter as Router , Route, Switch } from "react-router-dom"
import {GetInformation} from './components/getinformation/getinfo';


function App() {
  return (
    <div>
      <Router>
      
      <Route path="/" exact component={NavBar} />
      <Route path="/dashboard"  component={NavBar} />
      <Route path="/get-info"  component={GetInformation} />
      
      </Router>
   
    </div>
  );
}

export default App;

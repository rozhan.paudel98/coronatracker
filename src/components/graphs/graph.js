import React, { Component } from 'react'
import Chart from "react-google-charts";
import "./graph.css";

export default class Graph extends Component {
    render() {
        return (
            <div>
                <div className="graph">
                <Chart
  width={'500px'}
  height={'400px'}
  chartType="PieChart"
  loader={<div>Loading Chart</div>}
  data={[
    ['Task', 'Hours per Day'],
    ['Work', 1],
    ['Eat', 2],
    ['Commute', 2],
    ['Watch TV', 2],
    ['Sleep', 7],
  ]}
  options={{
    title: 'My Daily Activities',
    // Just add this option
    is3D: true,
  }}
  rootProps={{ 'data-testid': '2' }}
/>
                </div>
                
            </div>
        )
    }
}

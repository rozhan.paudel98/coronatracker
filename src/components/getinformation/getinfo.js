import React from 'react';
import "../NavBar/navbar.css";
import {Link} from "react-router-dom"

export  function GetInformation () {
    return (
        <div id="main-container">
        <div id="wrapper">

<div id="sidebar-wrapper">
<ul className="sidebar-nav">
<li><Link to="/dashboard">Dashboard</Link></li>
<li><Link to="/get-info">Get Information</Link></li>
<li><a href="#">Beta Version (By Roshan Paudel)</a></li>
</ul>
</div>


<div id="page-content-wrapper">
<div className="container-fluid">
<div className="row">
 <div className="col-lg-12 fellow">
   <button  class="btn" id="menu-toggle"><i class="fas fa-bars"></i></button>
   </div>
    </div>
    
    <div className="jumbotron jumbotron-fluid">
  <div className="container">
    <h1 className="display-4">NOVEL CORONA VIRUS</h1>
    <p className="lead">COVID-19 is a respiratory condition caused by a coronavirus. Some people are infected but don’t notice any symptoms. Most people will have mild symptoms and get better on their own. But about 1 in 6 will have severe problems, such as trouble breathing. The odds of more serious symptoms are higher if you’re older or have another health condition like diabetes or asthma.

Here’s what to look for if you think you might have COVID-19.

</p>
<h1 class="display-4">Symptoms</h1>
<p class="lead">The COVID-19 virus affects different people in different ways.  COVID-19 is a respiratory disease and most infected people will develop mild to moderate symptoms and recover without requiring special treatment.  People who have underlying medical conditions and those over 60 years old have a higher risk of developing severe disease and death.
<strong>
Common symptoms include:<br/>

1.   Fever<br/>
2.  Tiredness<br/>
3.  Dry cough.<br/>
Other symptoms include : <br/>

4.  Aches and pains<br/>
6.  Sore throat<br/>
and very few people will report diarrhoea, nausea or a runny nose.
</strong>
People with mild symptoms who are otherwise healthy should self-isolate and contact their medical provider or a COVID-19 information line for advice on testing and referral.

People with fever, cough or difficulty breathing should call their doctor and seek medical attention.

</p>
<h1 class="display-4">Preventions</h1>
<p class="lead">To prevent infection and to slow transmission of COVID-19, do the following:
<br/>
<strong>
1.  Wash your hands regularly with soap and water, or clean them with alcohol-based hand rub.<br/>
2.  Maintain at least 1 metre distance between you and people coughing or sneezing.<br/>
3.  Avoid touching your face.<br/>
4. Cover your mouth and nose when coughing or sneezing.<br/>
5. Stay home if you feel unwell.<br/>
6. Refrain from smoking and other activities that weaken the lungs.<br/>
7. Practice physical distancing by avoiding unnecessary travel and staying away from large groups of people.<br/>
</strong>
</p>
  </div>
</div>

 </div>
</div>
</div>

</div>


       
   
    )
}

import React, { Component } from 'react';
import "./counter.css";

export default class NumberCounter extends Component {
    constructor(props){
        super(props);
    }
    componentWillMount(){
        console.log("props is",this.props);
    }
    render() {
        return (
            <div>
                  <div className="sectiontitle container" >
    <h2> <i class="fas fa-virus"></i> COVID-19 Statistics</h2>
    <span className="headerLine"></span>
</div>
<div id="projectFacts" className="sectionclassName">
    <div className="fullWidth eight columns">
        <div className="projectFactsWrap ">
            <div className="item wow fadeInUpBig animated animated" data-number="12" style={{visibility: "visible"}}>
            <i class="fas fa-vial"></i>
        <p id="number1" className="number">{this.props.data.tested_total}</p>
                <span></span>
                <p>Total Tests</p>
            </div>
            <div className="item wow fadeInUpBig animated animated" data-number="55" style={{visibility: "visible"}}>
            <i class="fas fa-plus-circle"></i>
        <p id="number2" className="number">{this.props.data.tested_positive}</p>
                <span></span>
                <p>Positive</p>
            </div>
            <div className="item wow fadeInUpBig animated animated" data-number="359" style={{visibility: "visible"}}>
            <i class="fas fa-minus-circle"></i>
                <p id="number3" className="number">{this.props.data.tested_negative}</p>
                <span></span>
                <p>Negative</p>
            </div>
            <div className="item wow fadeInUpBig animated animated" data-number="246" style={{visibility: "visible"}}>
            <i class="fas fa-briefcase-medical"></i>
                <p id="number4" className="number">{this.props.data.recovered}</p>
                <span></span>
                <p>Recovered</p>
            </div>
            <br/>
            
            <br/>
        </div>
    </div>
</div>
            </div>
        )
    }
}

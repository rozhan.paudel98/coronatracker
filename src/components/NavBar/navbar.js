import React, { Component } from 'react';
import NumberCounter from "../counter/counter"
import "./navbar.css";
import axios from "axios"
import {api} from "../../utils/coronaapi";
import {Link} from "react-router-dom";


export default class NavBar extends Component {
    constructor(props){
        super(props);
        this.state={
            fetchedData:{},
            isLoading:false
        }
    }


    componentDidMount(){
      // console.log(this.props)
        this.setState({
            isLoading:true
        })

        axios.get(api)
            .then((data)=>{
                // console.log("fetched data is :",data);
                this.setState({
                    fetchedData: data.data
                })
            })
            .catch((error)=>{
                console.log("Error occured while loading the data");
            })
            .finally(()=>{
                this.setState({ isLoading:false})
            })
    }




    render() {
        const loader = this.state.isLoading 
        ? <div class="d-flex justify-content-center">
        <div class="spinner-border" role="status">
          <span class="sr-only">Loading...</span>
        </div>
      </div>
      : null;
        return (
            <div id="main-container">
                 <div id="wrapper">
    
                 <div id="sidebar-wrapper">
<ul className="sidebar-nav">
<li><Link to="/dashboard">Dashboard</Link></li>
<li><Link to="/get-info">Get Information</Link></li>
<li><a >Beta Version (By Roshan Paudel)</a></li>
</ul>
</div>
    
    
    <div id="page-content-wrapper">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-12 fellow">
            <button   className="btn" id="menu-toggle"><i class="fas fa-bars"></i></button>
            <h3 className="large text-center ">Beta Version</h3>
            <div className="overflow">
            <h2 className="small text-center">COUNTRY : NEPAL </h2>
            <div className="spinner">
            {loader}
            </div>
            
            <NumberCounter data={this.state.fetchedData}/>
            
            <br/><br/>
            <div className="row">
              <div className="col-sm-12">
              <h1 className="text-center show-box -2"> <i class="fas fa-skull-crossbones"></i> Deaths : {this.state.fetchedData.deaths}</h1>
        <h1 className="text-center show-box "> <i class="fab fa-algolia"></i> Pending Result : {this.state.fetchedData.pending_result}</h1>
               
              </div>

            
            </div>

            </div>
             
            
         
          </div>
        </div>
      </div>
      <p >Updated at : {this.state.fetchedData.updated_at}</p>
    </div>
    
  </div>

                
            </div>
        )
    }
}
